# Week 5 Mini-Project
## Author
Ziyu Shi
## Description
In this project, we are required to create a Rust AWS Lambda function (or app runner), implement a simple service and connect to a database. My choice is to connect my function to AWS MySQL database. My lambda function will read the content from the table in the database and calculate the frequencies of characters occuring in the attributes.

## Screenshots
### Connect to AWS MySQL
I create a database instance on AWS and connect to it from my local terminal. The name of the instance and the database in MySQL is 'miniproject5'
Use the following command to connect to a remote MySQL database:
```
mysql -h <HOST> -P <PORT> -u <YOUR_USER_NAME> -p
```
![image](images/dbConnect.png)

### Check the status of AWS Database
After connecting in the local terminal, we can check the Current acitvity in the database on AWS. There should be 1 Connections.
![image](images/awsConnect.png)

### Create and initialize table
Create a table named 'character_frequencies' and insert some data into it.
![image](images/dbContent.png)

### Connect to database and test the lambda function
Use `cargo run` to test the functionality of the lambda function.
![image](images/result.png)

## Create database instance in AWS
1. Go to the RDS service in AWS Console and create a new databse instance.
2. Ensure to modify the the publicly accessible as 'yes' and properly set the inbound rules in the VPC security groups.

## Deploy lambda function
1. Go to the IAM service in AWS Console and create a new role with the permissions `AWSLambda_FullAccess`, `IAMFullAccess` and `AmazonVPCCrossAccountNetworkInterfaceOperations`
2. Save your AWS secret key and use `aws configure` to set your key and region.
3. Use `cargo lambda build --release` to compile your code into binary file.
4. Use the following command to deploy your project onto AWS.
    ```
   cargo lambda deploy --region <YOUR_REGION> --iam-role <YOUR_ARN>
    ```
    Now you can go check your lambda function on AWS.