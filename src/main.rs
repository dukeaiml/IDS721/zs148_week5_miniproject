use std::collections::HashMap;
use mysql_async::{prelude::Queryable, Row, Pool};

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    // connect to AWS MySQL
    let pool = Pool::new("mysql://admin:12345678@miniproject5.c34ygcscohio.us-east-1.rds.amazonaws.com:3306/miniproject5");
    let mut conn = pool.get_conn().await?;

    // execute select
    let content_rows: Vec<Row> = conn.query("SELECT content FROM character_frequencies").await?;

    // calculate frequency
    let mut frequency_map = HashMap::new();
    for row in content_rows {
        if let Some(content) = row.get::<String, _>("content") {
            for c in content.chars() {
                *frequency_map.entry(c).or_insert(0) += 1;
            }
        }
    }

    // print result
    println!("Character frequency:");
    for (char, count) in frequency_map {
        println!("{}: {}", char, count);
    }

    Ok(())
}
